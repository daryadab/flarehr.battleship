﻿using flarehr.battleship.Enumerators;
using flarehr.battleship.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace flarehr.battleship.Models
{
    public class Ship
    {
        public readonly int Length;
        public readonly OrientationType Orientation;
        public readonly List<Square> OccupyingSquares;

        public Ship(int length, OrientationType orientation, string startingCol, int startingRow)
        {
            this.Length = length;
            this.Orientation = orientation;
            this.OccupyingSquares = new List<Square>();

            this.PlaceAt(startingCol, startingRow);
        } 

        public bool HasSunk 
        { 
            get 
            { 
                return this.OccupyingSquares.All(s=>s.AttackStatus == AttackStatusType.Hit); 
            } 
        }

        public void PlaceAt(string startingCol, int startingRow)
        {
            OccupyingSquares.Clear();
            int startingColInt = Converters.ColumnLetterToInt(startingCol);
            for (int i = 0; i < this.Length; i++)
            {
                if (this.Orientation == OrientationType.Vertical)
                    OccupyingSquares.Add(new Square(startingRow + i, startingCol));
                else
                    OccupyingSquares.Add(new Square(startingRow, Converters.IntToColumnLetter(startingColInt + i)));
            }
        }
    }
}
