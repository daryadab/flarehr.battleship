﻿using flarehr.battleship.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace flarehr.battleship.Models
{
    public class Square
    {
        public readonly int Row;
        public readonly string Col;
        public bool IsInBoard { get; set; }
        public bool Overlaps { get; set; }
        public Square(int row, string col)
        {
            this.Row = row;
            this.Col = col;
            this.IsInBoard = true;
            this.Overlaps = false;
        }
        public AttackStatusType AttackStatus { get; set; }
    }
}
