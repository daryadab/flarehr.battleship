﻿using flarehr.battleship.Enumerators;
using flarehr.battleship.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace flarehr.battleship.Models
{
    public class Board
    {
        public int RowCount { get; set; }
        public int ColCount { get; set; }
        public List<Ship> Ships { get; }
        public ImmutableList<Square> Squares { get; set; }
        public Board(int rowCount, int colCount)
        {
            this.RowCount = rowCount;
            this.ColCount = colCount;
            this.Ships = new List<Ship>();
            this.Squares = getEmptySquares(rowCount, colCount);
        }

        public bool HasLost()
        { 
            if (!Ships.Any())
                throw new Exception("No ship found");
            return this.Ships.All(s => s.HasSunk); 
        }

        public bool AddShip(Ship ship)
        {
            var outOfBoardSquares = ship.OccupyingSquares.Where(s => !IsSquareInBoard(s));

            // Check to ensure ship fits on board
            if (outOfBoardSquares.Any())
            {
                outOfBoardSquares.ToList().ForEach(s => s.IsInBoard = false);
                return false;
            }

            // Check to ensure the adding ship doesn't overlap another ship 
            foreach(var addedShip in this.Ships)
            {
                var overLappedSquares = ship.OccupyingSquares.Where(s => addedShip.OccupyingSquares.Any(os => os.Row == s.Row && os.Col == s.Col));
                if (overLappedSquares.Any())
                {
                    overLappedSquares.ToList().ForEach(s => s.Overlaps = true);
                    return false;
                }
            }

            this.Ships.Add(ship);

            return true;
        }

        public AttackStatusType AttackSquare( string position)
        {
            (int row, string col) = Converters.SquarePositionToRowCol(position);

            if (!IsSquareInBoard(new Square(row, col)))
                throw new Exception("Missed the board!");

            // update board square
            var boardSquare = this.Squares.First(os => os.Row == row && os.Col == col);
            boardSquare.AttackStatus = AttackStatusType.Hit;

            var shitHitSquare = Ships.SelectMany(s => s.OccupyingSquares).FirstOrDefault(os => os.Row == row && os.Col == col);
            if (shitHitSquare != null)
            {
                shitHitSquare.AttackStatus = AttackStatusType.Hit;

                return AttackStatusType.Hit;
            }


            return AttackStatusType.Miss;
        }

        public bool IsSquareInBoard(Square square)
        {
            return Squares.Any(s => s.Col == square.Col && s.Row == square.Row);
        }

        private static ImmutableList<Square> getEmptySquares(int rowCount, int colCount)
        {
            var builder = ImmutableList.CreateBuilder<Square>();

            for (int i = 0; i < rowCount; i++)
            {
                for (int j = 0; j < colCount; j++)
                    builder.Add(new Square(i + 1, Helpers.Converters.IntToColumnLetter(j + 1)));
            }

            return builder.ToImmutable(); ;
        }
    }
}
