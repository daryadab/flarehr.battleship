﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace flarehr.battleship.Helpers
{
    public static class Converters
    {
        public static string IntToColumnLetter(int columnNumber)
        {
            string columnLetter = string.Empty;

            while (columnNumber > 0)
            {
                int modulo = (columnNumber - 1) % 26;
                columnLetter = Convert.ToChar('A' + modulo) + columnLetter;
                columnNumber = (columnNumber - modulo) / 26;
            }

            return columnLetter;
        }
        public static int ColumnLetterToInt(string columnName)
        {
            int sum = 0;

            if (string.IsNullOrEmpty(columnName)) 
                throw new ArgumentNullException("columnName");

            columnName = columnName.ToUpperInvariant();


            for (int i = 0; i < columnName.Length; i++)
            {
                sum *= 26;
                sum += (columnName[i] - 'A' + 1);
            }

            return sum;
        }
        public static (int, string) SquarePositionToRowCol(string position)
        {
            int rowInt;
            string colString = string.Empty;

            if (string.IsNullOrWhiteSpace(position))
                throw new ArgumentNullException("position");

            if (!Char.IsLetter(position[0]))
                throw new Exception("Please provide col name followed by row number.");

            colString = new string (position.TakeWhile(x => Char.IsLetter(x)).ToArray());

            var splitted = position.Split(colString);

            if (splitted.Length != 2)
                throw new Exception("Wrong format");

            if (!Int32.TryParse(splitted[1], out rowInt))
                throw new Exception($"Unable to recognise row {splitted[1]}");

            return (rowInt, colString);
        }
    }
}
