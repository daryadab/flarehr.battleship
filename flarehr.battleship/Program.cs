﻿// See https://aka.ms/new-console-template for more information
using flarehr.battleship.Models;

var board = new Board(10, 10);

var ship = new Ship(3, flarehr.battleship.Enumerators.OrientationType.Horizontal, "A", 1);
var added = board.AddShip(ship);

Console.WriteLine(board.AttackSquare("A1"));

