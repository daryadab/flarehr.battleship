﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace flarehr.battleship.Enumerators
{
    public enum OrientationType
    {
        Horizontal,
        Vertical
    }
}
