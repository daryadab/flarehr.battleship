﻿Notes
There is a lot to talk about this task. There were assumptions made on the requirement, assumptions made on the design, assumptions on test requirements(just some simple examples coded up), whether values can be negatives(assumed disallowed)

Also, this project by no means is complete. There is a lot missing. Not all cases are coded up for, not all inputs are validated. While I usually don't like to place codes and logic in model classes, I did so in this project to prevent and avoid certain situations.

Happy to talk about all these and explain what has been done and what's the reasoning behind it.