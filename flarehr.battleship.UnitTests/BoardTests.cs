﻿using flarehr.battleship;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using flarehr.battleship.Models;
using flarehr.battleship.Enumerators;

namespace flarehr.battleship.UnitTests
{
    public class BoardTests
    {
        private Board testBoard;

        [SetUp]
        public void Setup()
        {
            this.testBoard = new Board(10, 10);
        }

        [Test]
        public void IsSquareInBoard_WhenInside_ReturnsTrue()
        {
            var expected = true;
            var actual = testBoard.IsSquareInBoard(new Square(3, "D"));
            Assert.AreEqual(expected, actual);
        }
        #region AttackSquare

        [Test]
        public void AttackSquare_WhenHits_ReturnsHit()
        {
            var expected = AttackStatusType.Hit;
            testBoard.AddShip(new Ship(3, OrientationType.Vertical, "A", 1));
            var actual = testBoard.AttackSquare("A1");
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void AttackSquare_WhenMisses_ReturnsMiss()
        {
            var expected = AttackStatusType.Miss;
            testBoard.AddShip(new Ship(3, OrientationType.Vertical, "A", 1));
            var actual = testBoard.AttackSquare("B1");
            Assert.AreEqual(expected, actual);
        } 
        #endregion

        #region HasLost

        [Test]
        public void HasLost_WhenNoShip_Throws()
        {
            var ex = Assert.Throws<Exception>(() => testBoard.HasLost());
            Assert.That(ex.Message, Is.EqualTo("No ship found"));
        }

        [Test]
        public void HasLost_WhenNoHitShip_ReturnsFalse()
        {
            testBoard.AddShip(new Ship(3, Enumerators.OrientationType.Horizontal, "A", 1));
            Assert.AreEqual(false, testBoard.HasLost());
        }

        [Test]
        public void HasLost_When1HitShip_ReturnsFalse()
        {
            testBoard.AddShip(new Ship(3, Enumerators.OrientationType.Vertical, "A", 1));
            testBoard.AttackSquare("A1");
            Assert.AreEqual(false, testBoard.HasLost());
        }

        [Test]
        public void HasLost_When2HitShip_ReturnsFalse()
        {
            testBoard.AddShip(new Ship(3, Enumerators.OrientationType.Vertical, "A", 1));
            testBoard.AttackSquare("A1");
            testBoard.AttackSquare("A2");
            Assert.AreEqual(false, testBoard.HasLost());
        }

        [Test]
        public void HasLost_When1ShipAddedAndSunk_ReturnsTrue()
        {
            testBoard.AddShip(new Ship(3, Enumerators.OrientationType.Vertical, "A", 1));
            testBoard.AttackSquare("A1");
            testBoard.AttackSquare("A2");
            testBoard.AttackSquare("A3");
            Assert.AreEqual(true, testBoard.HasLost());
        }

        [Test]
        public void HasLost_When2ShipAllHitOneLeft_ReturnsFalse()
        {
            var ship1 = new Ship(3, OrientationType.Vertical, "B", 2);
            var ship2 = new Ship(3, OrientationType.Vertical, "C", 1);
            testBoard.AddShip(ship1);
            var actual = testBoard.AddShip(ship2);

            testBoard.AttackSquare("B2");
            testBoard.AttackSquare("B3");
            testBoard.AttackSquare("B4");

            testBoard.AttackSquare("C1");
            testBoard.AttackSquare("C2");

            testBoard.AttackSquare("D3"); //Miss

            Assert.AreEqual(false, testBoard.HasLost());
        }

        [Test]
        public void HasLost_When2ShipAllHit_ReturnsTrue()
        {
            var ship1 = new Ship(3, OrientationType.Vertical, "B", 2);
            var ship2 = new Ship(3, OrientationType.Vertical, "C", 1);
            testBoard.AddShip(ship1);
            var actual = testBoard.AddShip(ship2);

            testBoard.AttackSquare("B2");
            testBoard.AttackSquare("B3");
            testBoard.AttackSquare("B4");

            testBoard.AttackSquare("C1");
            testBoard.AttackSquare("C2");
            testBoard.AttackSquare("C3");

            Assert.AreEqual(true, testBoard.HasLost());
        }

        #endregion

        #region AddShip

        [Test]
        public void AddShip_When1ShipAddedVerticallyOutside_ReturnsFalse()
        {
            var ship1 = new Ship(3, OrientationType.Vertical, "B", 9);
            var actual = testBoard.AddShip(ship1);

            Assert.AreEqual(false, actual);
        }

        [Test]
        public void AddShip_When1ShipAddedHorizontallyOutside_ReturnsFalse()
        {
            var ship1 = new Ship(3, OrientationType.Horizontal, "I", 2);
            var actual = testBoard.AddShip(ship1);

            Assert.AreEqual(false, actual);
        }

        [Test]
        public void AddShip_When1ShipAddedInside_ReturnsTrue()
        {
            var ship1 = new Ship(3, OrientationType.Horizontal, "B", 2);
            var actual = testBoard.AddShip(ship1);

            Assert.AreEqual(true, actual);
        }

        [Test]
        public void AddShip_When2ShipOverlap_ReturnsFalse()
        {
            var ship1 = new Ship(3, OrientationType.Horizontal, "B", 2);
            var ship2 = new Ship(3, OrientationType.Vertical, "C", 1);
            testBoard.AddShip(ship1);
            var actual = testBoard.AddShip(ship2);

            Assert.AreEqual(false, actual);
        }

        [Test]
        public void AddShip_When2ShipNoOverlap_ReturnsTrue()
        {
            var ship1 = new Ship(3, OrientationType.Horizontal, "B", 2);
            var ship2 = new Ship(3, OrientationType.Horizontal, "C", 1);
            testBoard.AddShip(ship1);
            var actual = testBoard.AddShip(ship2);

            Assert.AreEqual(true, actual);
        }
        #endregion

    }
}
