﻿using flarehr.battleship.Helpers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace flarehr.battleship.UnitTests
{
    public  class ConvertersTests
    {
        [SetUp]
        public void Setup()
        {
        }

        #region IntToColumnLetter

        [Test] public void IntToColumnLetter_When1_ReturnsA()   { var input = 1 ; var outputExpected = "A" ; string outputActual = Converters.IntToColumnLetter(input); Assert.AreEqual(outputExpected, outputActual); }
        [Test] public void IntToColumnLetter_When2_ReturnsB()   { var input = 2 ; var outputExpected = "B" ; string outputActual = Converters.IntToColumnLetter(input); Assert.AreEqual(outputExpected, outputActual); }
        [Test] public void IntToColumnLetter_When27_ReturnsAA() { var input = 27; var outputExpected = "AA"; string outputActual = Converters.IntToColumnLetter(input); Assert.AreEqual(outputExpected, outputActual); }
        [Test] public void IntToColumnLetter_When28_ReturnsAB() { var input = 28; var outputExpected = "AB"; string outputActual = Converters.IntToColumnLetter(input); Assert.AreEqual(outputExpected, outputActual); }
        [Test] public void IntToColumnLetter_When53_ReturnsBA() { var input = 53; var outputExpected = "BA"; string outputActual = Converters.IntToColumnLetter(input); Assert.AreEqual(outputExpected, outputActual); }
        #endregion

        #region ColumnLetterToInt

        [Test] public void ColumnLetterToInt_WhenA_Returns1() { var input = "A"; var outputExpected = 1; int outputActual = Converters.ColumnLetterToInt(input); Assert.AreEqual(outputExpected, outputActual); }
        [Test] public void ColumnLetterToInt_WhenB_Returns2() { var input = "B"; var outputExpected = 2; int outputActual = Converters.ColumnLetterToInt(input); Assert.AreEqual(outputExpected, outputActual); }
        [Test] public void ColumnLetterToInt_WhenAA_Returns27() { var input = "AA"; var outputExpected = 27; int outputActual = Converters.ColumnLetterToInt(input); Assert.AreEqual(outputExpected, outputActual); }
        [Test] public void ColumnLetterToInt_WhenAB_Returns28() { var input = "AB"; var outputExpected = 28; int outputActual = Converters.ColumnLetterToInt(input); Assert.AreEqual(outputExpected, outputActual); }
        [Test] public void ColumnLetterToInt_WhenBA_Returns53() { var input = "BA"; var outputExpected = 53; int outputActual = Converters.ColumnLetterToInt(input); Assert.AreEqual(outputExpected, outputActual); }

        #endregion

        #region SquarePositionToRowCol

        [Test] public void SquarePositionToRowCol_WhenA1_ReturnsAand1()     { var input = "A1"  ; var rowExpected = 1 ; var colExpected = "A" ; (int rowActual, string colActual) = Converters.SquarePositionToRowCol(input); Assert.AreEqual(rowExpected, rowActual); Assert.AreEqual(colExpected, colActual); }
        [Test] public void SquarePositionToRowCol_WhenC4_ReturnsCand4()     { var input = "C4"  ; var rowExpected = 4 ; var colExpected = "C" ; (int rowActual, string colActual) = Converters.SquarePositionToRowCol(input); Assert.AreEqual(rowExpected, rowActual); Assert.AreEqual(colExpected, colActual); }
        [Test] public void SquarePositionToRowCol_WhenAA11_ReturnsAAand11() { var input = "AA11"; var rowExpected = 11; var colExpected = "AA"; (int rowActual, string colActual) = Converters.SquarePositionToRowCol(input); Assert.AreEqual(rowExpected, rowActual); Assert.AreEqual(colExpected, colActual); }
        [Test] public void SquarePositionToRowCol_WhenA1A_ThrowsException() 
        { 
            var input = "A1A"; 

            var ex = Assert.Throws<Exception>(() => Converters.SquarePositionToRowCol(input));
            Assert.That(ex.Message, Is.EqualTo("Wrong format"));
        }
        [Test]
        public void SquarePositionToRowCol_WhenA11C_ThrowsException()
        {
            var input = "A11C";

            var ex = Assert.Throws<Exception>(() => Converters.SquarePositionToRowCol(input));
            Assert.That(ex.Message, Is.EqualTo("Unable to recognise row 11C"));
        }

        #endregion
    }
}
