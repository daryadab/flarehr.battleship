﻿using flarehr.battleship;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using flarehr.battleship.Models;

namespace flarehr.battleship.UnitTests
{
    public class ShipTests
    {
        private Board testBoard;
        private Ship testShip;

        [SetUp]
        public void Setup()
        {
            this.testBoard = new Board(10, 10);
            this.testShip = new Ship(3, Enumerators.OrientationType.Vertical, "A", 1);
            this.testBoard.AddShip(testShip);
        }

        [Test]
        public void HasSunk_When3HitShip_ReturnsTrue()
        {
            testBoard.AttackSquare("A1");
            testBoard.AttackSquare("A2");
            testBoard.AttackSquare("A3");

            Assert.AreEqual(true, testShip.HasSunk);
        }

        [Test]
        public void HasSunk_When1HitShip_ReturnsFalse()
        {
            testBoard.AttackSquare("A1");
            Assert.AreEqual(false, testShip.HasSunk);
        }

        [Test]
        public void HasLost_When2HitShip_ReturnsFalse()
        {
            testBoard.AttackSquare("A1");
            testBoard.AttackSquare("A2");
            Assert.AreEqual(false, testShip.HasSunk);
        }

        [Test]
        public void HasLost_When2Hit1MissShip_ReturnsFalse()
        {
            testBoard.AttackSquare("A1");
            testBoard.AttackSquare("A2");
            testBoard.AttackSquare("B2");
            Assert.AreEqual(false, testShip.HasSunk);
        }
    }
}
